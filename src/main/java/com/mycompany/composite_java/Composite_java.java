package com.mycompany.composite_java;
//Component Interface
import java.util.ArrayList;
import java.util.List;

interface Employee{
    String getName();
    double getSalary();
}
//Leaf Class
class Developer implements Employee{
    private String name;
    private double salary;
    public Developer(String name,float salary){
        this.name=name;
        this.salary=salary;
    }
    public String getName(){
        return name;
    }
    public double getSalary(){
        return salary;
    }
}
//Composit Class
class Department implements Employee{
    private String name;
    private List<Employee> employees=new ArrayList<>();
    public Department(String name){
        this.name=name;
    }
    public void addEmployee(Employee employee){
        employees.add(employee);
    }
    public String getName(){
        return name;
    }
    public double getSalary(){
        double totalSalary=0;
        for(Employee employee : employees){
            totalSalary+=employee.getSalary();
        }
        return totalSalary;
    }
}
public class Composite_java {
    public static void main(String[] args) {
        Employee developer1=new Developer("Jon Doe", 5000);
        Employee developer2=new Developer("Jane Smith", 7000);
        Department designDepartment=new Department("design Department");
        designDepartment.addEmployee(developer1);
        designDepartment.addEmployee(developer2);
        
        Employee manager =new Developer("manager", 8000);
        Department engineeringDEpartment=new Department("Engineering Department");
        engineeringDEpartment.addEmployee(manager);
        engineeringDEpartment.addEmployee(designDepartment);
        System.out.println("Total salary of Engineering Department: "+ engineeringDEpartment.getSalary());
    }
}
